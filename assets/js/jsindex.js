//
//  Scripts for the theme,
//  slideshow is used for Home Alt #4 (index4.html)
//  services is used for Services (services.html)
//
$(function () {
slideshow.initialize();
});
var slideshow = {
initialize: function () {
var $slideshow = $(".slideshow"),
$slides = $slideshow.find(".slide"),
$btnPrev = $slideshow.find(".btn-nav.prev"),
$btnNext = $slideshow.find(".btn-nav.next");
var index = 0;
var interval = setInterval(function () {
index++;
if (index >= $slides.length) {
index = 0;
}
updateSlides(index);
}, 4000);
$btnPrev.click(function () {
clearInterval(interval);
interval = null;
index--;
if (index < 0) {
index = $slides.length - 1;
}
updateSlides(index);
});
$btnNext.click(function () {
clearInterval(interval);
interval = null;
index++;
if (index >= $slides.length) {
index = 0;
}
updateSlides(index);
});
$slideshow.hover(function () {
$btnPrev.addClass("active");
$btnNext.addClass("active");
}, function () {
$btnPrev.removeClass("active");
$btnNext.removeClass("active");
});
function updateSlides(index) {
$slides.removeClass("active");
$slides.eq(index).addClass("active");
}
}
}
$('#sortrecentempresas').click(function() {
$('#recentempresas').show();
$('#popularempresas').hide();
$('#sortrecentempresas').addClass('active');
$('#sortviewsempresas').removeClass('active')
});
$('#sortviewsempresas').click(function() {
$('#popularempresas').show();
$('#recentempresas').hide();
$('#sortviewsempresas').addClass('active');
$('#sortrecentempresas').removeClass('active')
});
$('#sortrecentvideos').click(function() {
$('#recentvideos').show();
$('#popularvideos').hide();
$('#sortrecentvideos').addClass('selected');
$('#sortpopularvideos').removeClass('selected')
});
$('#sortpopularvideos').click(function() {
$('#popularvideos').show();
$('#recentvideos').hide();
$('#sortpopularvideos').addClass('selected');
$('#sortrecentvideos').removeClass('selected')
});
$("#wantcall").change(function() {
if(this.checked) {
$('#timecall').slideToggle();
}
else {
$('#timecall').hide();
}
});
$(document).ready(function() {
$("#owl1").owlCarousel({
loop:false,
items:3,
nav:true,
navText:["anterior","siguiente"],
responsiveClass:true,
responsive:{
0:{
items:1,
nav:true
},
600:{
items:3,
nav:false
},
1000:{
items:3,
nav:true,
loop:false
}
}
});
});
$(document).ready(function() {
$('.fancybox-media').fancybox({
openEffect  : 'elastic',
closeEffect : 'none',
helpers : {
media : {}
}
});
});
// $('#wordcloudBG').videoBG({
// position:"relative",
// zIndex:-1,
// mp4:'assets/mov/e1.mp4',
// webm:'assets/mov/e1.webm',
// ogv:'assets/mov/e1.ogv',
// poster:'assets/img/header.png',
// opacity:0.5,
// scale:true,
// });
$(document).ready(function() {
/*
var defaults = {
containerID: 'toTop', // fading element id
containerHoverID: 'toTopHover', // fading element hover id
scrollSpeed: 1200,
easingType: 'linear'
};
*/
$().UItoTop({ easingType: 'ease' });
});
// Disable stellar js
var isMobile = {
Android: function() {
return navigator.userAgent.match(/Android/i);
},
BlackBerry: function() {
return navigator.userAgent.match(/BlackBerry/i);
},
iOS: function() {
return navigator.userAgent.match(/iPhone|iPad|iPod/i);
},
Opera: function() {
return navigator.userAgent.match(/Opera Mini/i);
},
Windows: function() {
return navigator.userAgent.match(/IEMobile/i);
},
any: function() {
return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
}
};
jQuery(document).ready(function(){
if( !isMobile.any() )
$(function(){
$.stellar({
horizontalScrolling: false,
verticalOffset: 40
});
});
});
// Código para mostrar u ocultar cosas
$('#showempresas').click(function() {
$('#empresas').show();
$('#particulares').hide();
$('.arrow-down').css('left', '73.5%');
$('#empresastext').addClass('activechoose');
$('#particularestext').removeClass('activechoose');
$('#particularesicon').removeClass('fullopacity');
$('#empresaicon').addClass('fullopacity');
$('#triangulo').hide();
});
$('#showparticulares').click(function() {
$('#empresas').hide();
$('.arrow-down').css('left', '23.5%');
$('#particulares').show();
$('#empresastext').removeClass('activechoose');
$('#particularestext').addClass('activechoose');
$('#empresaicon').removeClass('fullopacity');
$('#particularesicon').addClass('fullopacity');
$('#triangulo').show();
});
// Código para los movimientos de las preguntas
						(function() {
						$(window).scroll(function() {
						var p1Val;
						p1Val = ($(window).scrollTop() + 10) / 3 ;
						return $("#pregunta1").css("right", p1Val);
						});
						}).call(this);
						(function() {
						$(window).scroll(function() {
						var p2Val;
						p2Val = ($(window).scrollTop() + 52) / 5 ;
						return $("#pregunta2").css("right", p2Val);
						});
						}).call(this);
						(function() {
						$(window).scroll(function() {
						var p3Val;
						p3Val = ($(window).scrollTop() + 5) / 5.9 ;
						return $("#pregunta3").css("right", p3Val);
						});
						}).call(this);
						(function() {
						$(window).scroll(function() {
						var p3Val;
						p3Val = ($(window).scrollTop() + 100) / 8 ;
						return $("#pregunta4").css("left", p3Val);
						});
						}).call(this);
						(function() {
						$(window).scroll(function() {
						var p5Val;
						p5Val = ($(window).scrollTop() + 20) / 4 ;
						return $("#pregunta5").css("left", p5Val);
						});
						}).call(this);
						(function() {
						$(window).scroll(function() {
						var p6Val;
						p6Val = ($(window).scrollTop() - 20) / 13 ;
						return $("#pregunta6").css("right", p6Val);
						});
						}).call(this);
						(function() {
						$(window).scroll(function() {
						var p7Val;
						p7Val = ($(window).scrollTop() - 30) / 7 ;
						return $("#pregunta7").css("left", p7Val);
						});
						}).call(this);
						(function() {
						$(window).scroll(function() {
						var p8Val;
						p8Val = ($(window).scrollTop() + 52) / 13 ;
						return $("#pregunta8").css("right", p8Val);
						});
						}).call(this);
						(function() {
						$(window).scroll(function() {
						var p9Val;
						p9Val = ($(window).scrollTop() + 52) / 13 ;
						return $("#pregunta9").css("left", p9Val);
						});
						}).call(this);
						(function() {
					$(window).scroll(function() {
					var oVal;
					oVal = ($(window).scrollTop() - 240) / 300;
					return $("#pregunta1").css("opacity", oVal);
					});
					}).call(this);
						(function() {
					$(window).scroll(function() {
					var oVal;
					oVal = ($(window).scrollTop() - 200) / 300;
					return $("#pregunta2").css("opacity", oVal);
					});
					}).call(this);
						(function() {
					$(window).scroll(function() {
					var oVal;
					oVal = ($(window).scrollTop() - 270) / 300;
					return $("#pregunta3").css("opacity", oVal);
					});
					}).call(this);
						(function() {
					$(window).scroll(function() {
					var oVal;
					oVal = ($(window).scrollTop() - 100) / 300;
					return $("#pregunta4").css("opacity", oVal);
					});
					}).call(this);
						(function() {
					$(window).scroll(function() {
					var oVal;
					oVal = ($(window).scrollTop() - 240) / 300;
					return $("#pregunta5").css("opacity", oVal);
					});
					}).call(this);
						(function() {
					$(window).scroll(function() {
					var oVal;
					oVal = ($(window).scrollTop() - 224) / 300;
					return $("#pregunta6").css("opacity", oVal);
					});
					}).call(this);
						(function() {
					$(window).scroll(function() {
					var oVal;
					oVal = ($(window).scrollTop() - 140) / 300;
					return $("#pregunta7").css("opacity", oVal);
					});
					}).call(this);
						(function() {
					$(window).scroll(function() {
					var oVal;
					oVal = ($(window).scrollTop() - 52) / 300;
					return $("#pregunta8").css("opacity", oVal);
					});
					}).call(this);
						(function() {
					$(window).scroll(function() {
					var oVal;
					oVal = ($(window).scrollTop() - 290) / 300;
					return $("#pregunta9").css("opacity", oVal);
					});
					}).call(this);





