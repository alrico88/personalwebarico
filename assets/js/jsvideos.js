$('#titlefinanzas').waypoint(function() {
$('#lifinanzas').parent().addClass('active');
$(".selectitem:not(#lifinanzas)").parent().removeClass('active');
}, { offset: 129 });
$('#cmatrimonial').waypoint(function() {
$('#limatrimonial').parent().addClass('active');
$(".selectitem:not(#limatrimonial)").parent().removeClass('active');
}, { offset: 122 });
$('#cempresas').waypoint(function() {
$('#liempresas').parent().addClass('active');
$(".selectitem:not(#liempresas)").parent().removeClass('active');
}, { offset: 122 });
$('#cviviendas').waypoint(function() {
$('#liviviendas').parent().addClass('active');
$(".selectitem:not(#liviviendas)").parent().removeClass('active');
}, { offset: 122 });
$('#claboral').waypoint(function() {
$('#lilaboral').parent().addClass('active');
$(".selectitem:not(#lilaboral)").parent().removeClass('active');
}, { offset: 122 });
$('#ccontratos').waypoint(function() {
$('#licontratos').parent().addClass('active');
$(".selectitem:not(#licontratos)").parent().removeClass('active');
}, { offset: 122 });
$('#cpenal').waypoint(function() {
$('#lipenal').parent().addClass('active');
$(".selectitem:not(#lipenal)").parent().removeClass('active');
}, { offset: 122 });

// PARA EL CAROUSEL

$('.owl-carousel').owlCarousel({
    center: true,
    loop:false,
    nav:true,
    navText:["< Anterior","Siguiente >"],
    margin:10,
    responsive:{
        0:{
            items:2,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:5,
            nav:true,
            loop:false
        }
    }
});